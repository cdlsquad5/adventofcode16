package star1;

import java.util.ArrayList;
import java.util.List;

public class StartEndPositionVector {

    private List<Integer> startingPoint;

    private List<Integer> endPoint;

    private int dimension;

    private Enum facingDirection;

    public StartEndPositionVector(int dimension) {
        this.dimension = dimension;
        startingPoint = new ArrayList<>(dimension);
        endPoint = new ArrayList<>(dimension);
        facingDirection = FacingDirections.North;
    }

    public int getDimension() {
        return dimension;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
    }

    public void setStartingPoint(List<Integer> startingPoint) {
        this.startingPoint = startingPoint;
    }

    public void setEndPoint(List<Integer> endPoint) {
        this.endPoint = endPoint;
    }

    public List<Integer> getStartingPoint() {
        return startingPoint;
    }

    public List<Integer> getEndPoint() {
        return endPoint;
    }

    public Enum getFacingDirection() {
        return facingDirection;
    }

    public void setFacingDirection(Enum facingDirection) {
        this.facingDirection = facingDirection;
    }

    public int calculateTaxiCabDistance() {
        int distance = 0;
        for (int i = 0; i < dimension; i++) {
            distance += Math.abs(endPoint.get(i) - startingPoint.get(i));
        }
        return distance;
    }

}
