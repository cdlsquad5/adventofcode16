package star1;

import java.util.ArrayList;
import java.util.List;

public class CalculateDistance {

    String instructions = "R1, L3, R5, R5, R5, L4, R5, R1, R2, L1, L1, R5, R1, L3, L5, L2, R4, L1, R4, R5, L3, R5, L1, R3, L5, R1, L2, R1, L5, L1, R1, R4, R1, L1, L3, R3, R5, L3, R4, L4, R5, L5, L1, L2, R4, R3, R3, L185, R3, R4, L5, L4, R48, R1, R2, L1, R1, L4, L4, R77, R5, L2, R192, R2, R5, L4, L5, L3, R2, L4, R1, L5, R5, R4, R1, R2, L3, R4, R4, L2, L4, L3, R5, R4, L2, L1, L3, R1, R5, R5, R2, L5, L2, L3, L4, R2, R1, L4, L1, R1, R5, R3, R3, R4, L1, L4, R1, L2, R3, L3, L2, L1, L2, L2, L1, L2, R3, R1, L4, R1, L1, L4, R1, L2, L5, R3, L5, L2, L2, L3, R1, L4, R1, R1, R2, L1, L4, L4, R2, R2, R2, R2, R5, R1, L1, L4, L5, R2, R4, L3, L5, R2, R3, L4, L1, R2, R3, R5, L2, L3, R3, R1, R3";

    String example1 = "R5, L5, R5, R3";

    String example2 = "R2, R2, R2";

    String example3 = "R2, L3";

    String example4 = "R5, R40, L3";

    private StartEndPositionVector positionVector;

    private EndPointCalculator endPointCalculator;

    public CalculateDistance() {
        positionVector = new StartEndPositionVector(2);
        endPointCalculator = new EndPointCalculator(instructions, 2);
    }

    public int calculateDistance() {
        setStartingPoint();
        positionVector.setEndPoint(endPointCalculator.calculateEndPointBeingTheStartingPoint(positionVector.getStartingPoint()));
        return positionVector.calculateTaxiCabDistance();
    }

    private void setStartingPoint() {
        List<Integer> startingPoint = new ArrayList<>();
        startingPoint.add(0);
        startingPoint.add(0);
        positionVector.setStartingPoint(startingPoint);
    }

    public static void main(String[] args) {
        CalculateDistance calculateDistance = new CalculateDistance();
        System.out.println(calculateDistance.calculateDistance());
    }

}
