package star1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EndPointCalculator {

    private String instructions;

    StartEndPositionVector positionVector;

    public EndPointCalculator(String instructions, int dimension) {
        this.instructions = instructions;
        positionVector = new StartEndPositionVector(dimension);
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public List<Integer> calculateEndPointBeingTheStartingPoint(List<Integer> startingPoint) {
        positionVector.setStartingPoint(startingPoint);
        positionVector.setEndPoint(createEndPoint());
        List<String> tokens = tokenizeInstructions();

        for (int i = 0; i < tokens.size(); i++) {
            applyInstructionToEndPoint(tokens.get(i), i);
        }
        return positionVector.getEndPoint();
    }

    private List<Integer> createEndPoint() {
        List<Integer> endPoint = new ArrayList<>(positionVector.getDimension());
        endPoint.add(0);
        endPoint.add(0);
        return endPoint;
    }

    private void applyInstructionToEndPoint(String token, int instructionNumber) {
        positionVector.setFacingDirection(getNewFacingDirection(token.charAt(0)));
        if (instructionNumber % 2 == 0) {
            positionVector.getEndPoint().set(0, applyDistanceToFirstAxe(token.substring(1)));
        } else {
            positionVector.getEndPoint().set(1, applyDistanceToSecondAxe(token.substring(1)));
        }

    }

    private Integer applyDistanceToFirstAxe(String distance) {
        if (FacingDirections.Est.equals(positionVector.getFacingDirection())) {
            return positionVector.getEndPoint().get(0) + Integer.parseInt(distance);
        } else {
            return positionVector.getEndPoint().get(0) - Integer.parseInt(distance);
        }
    }

    private Integer applyDistanceToSecondAxe(String distance) {
        if (FacingDirections.North.equals(positionVector.getFacingDirection())) {
            return positionVector.getEndPoint().get(1) + Integer.parseInt(distance);
        } else {
            return positionVector.getEndPoint().get(1) - Integer.parseInt(distance);
        }
    }

    private Enum getNewFacingDirection(char turnDirection) {
        if (FacingDirections.North.equals(positionVector.getFacingDirection())) {
            if (turnDirection == 'R') {
                return FacingDirections.Est;
            } else {
                return FacingDirections.West;
            }
        }
        if (FacingDirections.South.equals(positionVector.getFacingDirection())) {
            if (turnDirection == 'R') {
                return FacingDirections.West;
            } else {
                return FacingDirections.Est;
            }
        }
        if (FacingDirections.Est.equals(positionVector.getFacingDirection())) {
            if (turnDirection == 'R') {
                return FacingDirections.South;
            } else {
                return FacingDirections.North;
            }
        }
        if (FacingDirections.West.equals(positionVector.getFacingDirection())) {
            if (turnDirection == 'R') {
                return FacingDirections.North;
            } else {
                return FacingDirections.South;
            }
        }
        return null;
    }

    private List<String> tokenizeInstructions() {
        return Arrays.asList(instructions.split(", "));
    }
}
